#!/usr/bin/python2

from __future__ import division, print_function

from flup.server.fcgi import WSGIServer
import flask
import logging
from logging.handlers import RotatingFileHandler

import pystache
from markdown import markdown
import re
import tex

import os

BASE = "/"
TEMPLATES = "templates"

LATEX = r"(?:^|[^\$])\$([^\$]+)\$"

app = flask.Flask(__name__)
app.config["UPLOAD_FOLDER"] = "/tmp"

def render(page=None, location=TEMPLATES):
	def render_response(code, template, content):
		document = template.render_name("index.html", {"base": BASE, "title": "Personal Site of Martin Kunev", "content": content})

		response = flask.Response(document, code)
		response.headers["Content-Type"] = "text/html";
		return response

	def render_formulas(content):
		for code in re.findall(LATEX, content, flags=re.DOTALL):
			formula = tex.export(code.encode("UTF-8"))

			inner = None
			if formula:
				if (code[0] == "\n") and (code[-1] == "\n"):
					inner = '<img src="' + formula + '" alt="formula" />'
				else:
					inner = '<img src="' + formula + '" alt="formula" class="inline" />'
			else:
				inner = "cannot display formula"
			content = content.replace("${}$".format(code), inner.decode("UTF-8"))
		return content.replace("$$", "$")

	template = pystache.Renderer(file_encoding="UTF-8", search_dirs=TEMPLATES, file_extension=False)

	# IE not supported.
	agent = flask.request.headers.get('User-Agent')
	if agent and ((agent.find("Trident") >= 0) or (agent.find("Edge") >= 0) or (agent.find("MSIE") >= 0)):
		return render_response(400, template, "<b>browser not supported</b>")

	if not page:
		page = "main.markdown"

	try:
		# Load page content and remove the terminating line feed.
		content = pystache.Renderer(file_encoding="UTF-8", search_dirs=location, file_extension=False).render_name(page)[:-1]

		# Remove comments.
		content = re.sub(r"<!--.*?-->", "", content, flags=re.DOTALL)

		# Render embedded LaTeX.
		content = render_formulas(content)

		# Convert markdown to HTML.
		content = markdown(content, extensions=["markdown.extensions.tables", "markdown.extensions.nl2br"])

		# Make all links open in new tab.
		content = re.sub('<a href="http(s)?://', '<a target="_blank" href="http\\1://', content)
	except Exception as exception:
		app.logger.error(str(exception))
		return render_response(404, template, "<b>not found</b>")

	return render_response(200, template, content)

@app.route("/", methods=["GET"])
def index():
	return render()

@app.route("/<page>", methods=["GET"])
def article(page):
	return render(page)

@app.route("/lessons/<page>", methods=["GET"])
def lesson(page):
	return render(page, TEMPLATES + "/lessons")

if __name__ == '__main__':
	log_handler = RotatingFileHandler('flask.log', maxBytes=1024000, backupCount=1)
	log_handler.setLevel(logging.INFO)
	app.logger.addHandler(log_handler)

	WSGIServer(app).run()
