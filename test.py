#!/usr/bin/python2

import re

from index import LATEX

def test_regex():
	print(re.findall(LATEX, "$a = 1$", flags=re.DOTALL))
	print(re.findall(LATEX, "$$a = 1$$", flags=re.DOTALL))
	print(re.findall(LATEX, " $a = 1$ ", flags=re.DOTALL))
	print(re.findall(LATEX, "$a = 1$ and $b = 2$", flags=re.DOTALL))

	assert re.findall(LATEX, "$a = 1$", flags=re.DOTALL) == ["a = 1"]
	assert re.findall(LATEX, "$$a = 1$$", flags=re.DOTALL) == []
	assert re.findall(LATEX, " $a = 1$ ", flags=re.DOTALL) == ["a = 1"]
	assert re.findall(LATEX, "$a = 1$ and $b = 2$", flags=re.DOTALL) == ["a = 1", "b = 2"]

test_regex()
