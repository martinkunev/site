#!/usr/bin/python2

import os
import string
import base64
from threading import Lock

import pymmh3

mutex = Lock()

def to_bytes(n, length, endianess="big"):
    h = '%x' % n
    s = ('0'*(len(h) % 2) + h).zfill(length * 2).decode('hex')
    return s if endianess == "big" else s[::-1]

def hash_base64(data):
	BASE64 = b"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"
	ENC64 = b"0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-_"

	number = pymmh3.hash(data)
	hashed = to_bytes(number if (number >= 0) else (number + 2 ** 64), 8)
	table = string.maketrans(BASE64, ENC64)
	return str(base64.standard_b64encode(hashed).rstrip(b"=").translate(table))

def export(tex):
	tex = tex.strip()
	OUTPUT = "/static/cache/" + hash_base64(tex) + ".svg"
	if os.access("/var/www" + OUTPUT, os.F_OK):
		return OUTPUT

	PREFIX = "/tmp/www_tex"

	TEMP_TEX = PREFIX + "/export.tex"
	TEMP_DVI = PREFIX + "/export.dvi"

	mutex.acquire()

	# Create a directory where texi2dvi can put its junk.
	# Due to bugs in texi2dvi, write access to current directory is necessary.
	try:
		os.makedirs(PREFIX)
	except OSError:
		pass
	cwd = os.getcwd()
	os.chdir(PREFIX)

	f = open(TEMP_TEX, "w")
	f.write(
"""\\documentclass[a4paper,11pt,fleqn,draft]{article}
\\pagenumbering{gobble}
\\usepackage{amsmath,amsfonts,amssymb}
\\usepackage[%
    left=0.50in,%
    right=0.50in,%
    top=1.0in,%
    bottom=1.0in,%
    paperheight=11in,%
    paperwidth=8.5in%
]{geometry}%
\\begin{document}
""")
	f.write("\\begin{equation*}" + tex + " \\end{equation*}\n");
	f.write("\\end{document}\n")
	f.close()

	try:
		os.unlink(TEMP_DVI)
	except OSError:
		pass
	os.system("texi2dvi " + TEMP_TEX)
	if os.path.isfile(TEMP_DVI):
		os.unlink(TEMP_TEX)
		os.system("dvisvgm -b 1 --no-fonts -o " + "/var/www" + OUTPUT + " " + TEMP_DVI)
		os.unlink(TEMP_DVI)
	else:
		OUTPUT = None

	mutex.release()

	os.chdir(cwd)

	return OUTPUT

#export("x_0 = 5")
