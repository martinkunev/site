Personal website based on Python Flask.

* Renders mustache templates.

* Loads files consisting of markdown and LaTeX formulas.

LaTeX formulas are converted to SVG images and copied into `/static/cache`

The site has been tested with spawn-fcgi.

# Installation

Place the contents of the repository in `/var/www`

For nginx, add the following in the http section:

    server {
        listen 80;
        server_name my_awesome_server;

        access_log /var/log/nginx/access_log main;
        error_log /var/log/nginx/error_log debug;

        location ~ /static/ {
            root /var/www;
        }
        location / {
            include /etc/nginx/fastcgi_params;
            fastcgi_pass 127.0.0.1:8080;
        }
    }

For LaTeX, the executables texi2dvi and dvisvgm are required.

On Debian/Devuan:

	# apt-get install texinfo dvisvgm

On Gentoo:

	# emerge sys-apps/texinfo-6.1 app-text/dvisvgm

## Usage

Place templates containing markdown and LaTeX in `/templates`. See main.markdown for an example.
The directory `/static` is for files you want to be served directly.
The LaTeX content is added to web pages as SVG images. These images are cached in `/static/cache`.
